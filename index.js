var WebSocketServer = require('ws').Server;
var connect = require('connect');
var serveStatic = require('serve-static');
var DataView = require('buffer-dataview');

var Game = require('./server/Game.js');
var Player = require('./server/Player.js');
var Cell = require('./server/Cell.js');
var Vector = require('./server/Vector.js');
var Observer = require('./server/Observer.js');


/*
 *	Création du jeu
 */
var game = new Game();
game.exec();

/*
 * Chargement des client
 */
connect().use(serveStatic(__dirname + '/client')).listen(80);

/*
 * Connexion par web socket
 */
var wss = new WebSocketServer({port: 8080});
wss.on('connection', function(ws) {

	var player = game.createPlayer(ws);

	ws.on('close', function() {
		player.disconnect();
	});

	ws.on('error', function() {
		player.disconnect();
	})

	ws.on('message', function(message) {
		var dataView = new DataView(message);
		var header = dataView.getUint8(0); var c = 1;

		// Jouer
		if(header == 1) {
			if(!player.isPlaying) {
				var name = '';
				while(dataView.getUint8(c) !== 0) {
					name += String.fromCharCode(dataView.getUint8(c)); c += 1;
				}
				player.setName(name);
				player.play();
			}
		}
		// Mouse direction changed
		else if(header == 16) {
			if(player.isPlaying) {
				var x = dataView.getInt32(1) / 100;
				var y = dataView.getInt32(5) / 100;
				player.setMouseDirection(new Vector(x, y));
			}
		}
		// Split
		else if(header == 17) {
			if(player.isPlaying)
				player.split();
		}
		// Feed
		else if(header == 18) {
			if(player.isPlaying)
				player.feed();
		}
	});
});