function Game(canvas, leaderboard, menu, splitButton, feedButton, menuButton, resizeButton) {

	this.canvas = canvas;
	this.context = canvas.getContext('2d');
	this.leaderboard = leaderboard;
	this.menu = menu;
	this.splitButton = splitButton;
	this.feedButton = feedButton;
	this.menuButton = menuButton;
	this.resizeButton = resizeButton;

	this.cells = [];
	this.cellsById = {};
	this.myCells = [];

	this.players = [];
	this.playersById = {};
	this.leaders = [];

	this.isPlaying = false;
	this.isObserving = true;
	this.isPaused = true;
	this.webSocket;
	this.width = 0;
	this.height = 0;
	this.nbrFrame = 0;

	this.oldCenterX = 0;
	this.oldCenterY = 0;
	this.oldRadius = 0;

	this.updateTime = new Date();
	this.displayTime = new Date();
	this.nbrPacketsReceived = 0;
	this.latency = 80;

	// Device detection
	this._deviceDetection();

	// Envoi des données
	this.sendTimeout = null;
	this.sendTime = new Date();

	// Init inputs
	this._initInputs();

	// Init web socket
	this._initWebSocket();

	// Init interface
	this.playersReceived = false;
	this.cellsReceived = false;
	this.interfaceInitialized = false;
	// this._initInterface();
}

// Device detection
Game.prototype._deviceDetection = function() {
	var userAgent = navigator.userAgent || navigator.vendor || window.opera;
	this.isIphone = false;
	this.isAndroid = false;
	this.isComputer = false;

	if(userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
		this.isIphone = true;
	}
	else if(userAgent.match(/Android/i)) {
		this.isAndroid = true;
	}
	else {
		this.isComputer = true;
	}
}

// Input connection
Game.prototype._initInputs = function() {
	var that = this;

	// Ordinateur
	if(this.isComputer) {
		// Déplacement de la souris
		this.canvas.addEventListener('mousemove', function(e) {
			that.updateMouseDirection(
				(e.clientX - that.canvas.width / 2) / that.scaleX,
				(e.clientY - that.canvas.height / 2) / that.scaleY
			);
		});
		// Appui sur...
		document.onkeydown = function(e) {
			// Espace
			if(e.keyCode === 32) {
				that.split();
			}
			// W
			else if(e.keyCode === 87) {
				that.feed();
			}
			// Escape
			else if(e.keyCode === 27) {
				that.tooglePause();
			}
			// 
			else if(e.keyCode === 65 && that.isComputer) {
				document.getElementById('infos').style.display = 'block';
			}
		};
	};
	// Iphone + Android
	if(this.isIphone || this.isAndroid) {
		// Posage du doigt
		this.canvas.addEventListener('touchstart', function(e) {
			that.touchX = e.changedTouches[0].clientX;
			that.touchY = e.changedTouches[0].clientY;
			that.updateMouseDirection(0, 0);
		});
		// Déplacement du doigt
		this.canvas.addEventListener('touchmove', function(e) {
			that.updateMouseDirection(
				(e.changedTouches[0].clientX - that.touchX) / that.scaleX * 2,
				(e.changedTouches[0].clientY - that.touchY) / that.scaleY * 2
			);
		});
		// Appuis sur le bouton split
		this.splitButton.addEventListener('touchstart', function(e) {
			that.split();
		});
		// Appuis sur le bouton feed
		this.feedButton.addEventListener('touchstart', function(e) {
			that.feed();
		});
		// Appuis sur le bouton feed
		this.menuButton.addEventListener('touchstart', function(e) {
			that.tooglePause();
		});
		// Appuis sur le bouton resize
		this.resizeButton.addEventListener('touchstart', function(e) {
			that.resize();
		})
		// Désactiver
		document.ontouchmove = function(event){
			event.preventDefault();
		};
		window.addEventListener('orientationchange', function () {
			that.resize();
			setTimeout(function() {
				that.resize();
			}, 1000);
		});
	}
	// Redimensionnement de la fenêtre
	window.addEventListener('resize', function(e) {
		that.resize();
	});
}

// Socket connection
Game.prototype._initWebSocket = function() {
	this.webSocket = new WebSocket('ws://' + window.location.hostname + ':8080');
	this.webSocket.binaryType = 'arraybuffer';

	var that = this;

	this.webSocket.onopen = function() {

	};

	this.webSocket.onmessage = function(message) {
		that.onReceiveBuffer(message.data)
	}
}

// Interface initialisation
Game.prototype._initInterface = function() {
	this.resize();

	var that = this;
	window.requestAnimationFrame(function() { that.display(); });
	if(!this.isComputer)
		this.latency = 120;
	//this.display();

	if(this.isComputer) {
		this.splitButton.style.display = 'none';
		this.feedButton.style.display = 'none';
		this.menuButton.style.display = 'none';
	}
	if(!this.isAndroid) {
		this.resizeButton.style.display = 'none';
	}
}

// Redimensionnement de la map
Game.prototype.resize = function() {
	// Resize du canvas
	if(this.isAndroid) {
		this.canvas.width = window.outerWidth;
		this.canvas.height = window.outerHeight;
	}
	else {
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
	}

	// Resize des boutons de split et feed
	if(this.isAndroid || this.isIphone) {
		var size = Math.min(this.canvas.width, this.canvas.height);
		var s20 = (0.2 * size) + 'px',
			s10 = (0.1 * size) + 'px',
			s19 = (0.19 * size) + 'px';
		this.splitButton.style.width = s20;
		this.splitButton.style.height = s20;
		this.splitButton.style.lineHeight = s19;
		this.splitButton.style.borderRadius = s10;
		this.splitButton.style.right = s20;
		this.feedButton.style.width = s20;
		this.feedButton.style.height = s20;
		this.feedButton.style.lineHeight = s19;
		this.feedButton.style.borderRadius = s10;
		this.feedButton.style.bottom = s20;
		this.menuButton.style.width = s20;
		this.menuButton.style.height = s20;
		this.menuButton.style.lineHeight = s19;
		this.menuButton.style.borderRadius = s10;
		this.menuButton.style.bottom = s20;
	}
}

// Lancement du jeu
Game.prototype.play = function(name) {
	this.isPaused = false;

	if(this.webSocket.readyState === 1 && !this.isPlaying) {
		// Envoi d'une requête de début de jeu
		var nameLength = Math.min(name.length, 32);
		var bufferLength = nameLength + 2;
		var dataView = new DataView(new ArrayBuffer(bufferLength));
		dataView.setUint8(0, 1);
		for(var k = 0; k < nameLength; k++)
			dataView.setUint8(k + 1, name.charCodeAt(k));
		dataView.setUint8(nameLength + 1, 0);
		this.webSocket.send(dataView.buffer);
		// Initialisation de la map
		this.isPlaying = true;
		this.isObserving = false;
	}
};

// Lancement du mode observateur
Game.prototype.observe = function() {
	this.isPaused = false;

	if(!this.isPlaying) {
		this.isObserving = true;
	}
};

// Initialisation du jeu
Game.prototype.init = function() {
	this.width = 0;
	this.height = 0;

	this.oldCenterX = 0;
	this.oldCenterY = 0;
	this.oldRadius = 0;

	this.updateTime = new Date();
	this.displayTime = new Date();
	this.latency = 100;
};

// Toogle pause
Game.prototype.tooglePause = function() {
	if(!this.isPaused) {
		this.pause();
	}
	else {
		this.unpause();
	}
}

// Mettre le jeu en pause
Game.prototype.pause = function() {
	this.isPaused = true;
	this.showMenu();
}

// Annuler la pause du jeu
Game.prototype.unpause = function() {
	this.isPaused = false;
	if(this.isPlaying) {
		this.hideMenu();
	}
	else if(this.isObserving) {
		this.hideMenu();
	}
}

// Masquer le menu
Game.prototype.hideMenu = function() {
	this.menu.style.display = 'none';
};

// Afficher le menu
Game.prototype.showMenu = function() {
	this.menu.style.display = 'block';
};

// Afficher le jeu
Game.prototype.display = function() {
	this.nbrFrame++;

	// Variables qu'il faut redéclarer à chaque tour (:[)
	var currentTime = new Date();
	var that = this;

	var cell, i, j;
	// Refresh des données des cellules
	for(i = 0; i < this.cells.length; i++) {
		cell = this.cells[i];
		cell.refresh(currentTime);

		// Suppression des cellules mortes
		if(!cell.isAlive && cell.timeSinceDeath() > this.latency) {
			delete this.cellsById[cell.id];
			this.cells.splice(i--, 1);
			var p = this.myCells.indexOf(cell);
			if(p !== -1)
				this.myCells.splice(p, 1);
		}
	}

	var centerX = 0,
		centerY = 0;
	if(this.isPlaying) {
		// Calcul du barycentre et de la masse
		var center
		var nbrAliveCells = 0;
		var nbrMyCells = this.myCells.length;

		var mass = 0;
		for(i = 0; i < nbrMyCells; i++) {
			cell = this.myCells[i];
			if(cell.isAlive) {
				nbrAliveCells += 1;
				centerX += cell.x;
				centerY += cell.y;
				mass += cell.mass;
			}
		}
		if(nbrAliveCells !== 0) {
			centerX /= nbrAliveCells;
			centerY /= nbrAliveCells;
		}
		if(nbrAliveCells === 0) {
			centerX = this.oldCenterX;
			centerY = this.oldCenterY;
		}
		this.oldCenterX = centerX;
		this.oldCenterY = centerY;

		// Calculs pour l'affichage
		var radius = Math.sqrt(mass / Math.PI);
		if(radius === 0)
			radius = this.oldRadius;
		else
			this.oldRadius = radius;
		var canvasRatio = this.canvas.height / this.canvas.width;
		var width = 16 * radius;
		var height = width * canvasRatio;
		this.scaleX = this.canvas.width / width;
		this.scaleY = this.canvas.height / height;
	}
	else if(this.isObserving) {
		var maxMass = 0;
		for(i = 0; i < this.cells.length; i++) {
			if(this.cells[i].mass > maxMass)
				maxMass = this.cells[i].mass;
		}
		var maxRadius = Math.sqrt(maxMass / Math.PI);
		var width = this.width + maxRadius * 2;
		var height = this.height + maxRadius * 2;
		var scaleX = this.canvas.width / width,
			scaleY = this.canvas.height / height;
		var canvasRatio = this.canvas.height / this.canvas.width;

		if(scaleX < scaleY) {
			width = width;
			height = width * canvasRatio;
		}
		else {
			height = height;
			width = height / canvasRatio;
		}
		this.scaleX = this.canvas.width / width;
		this.scaleY = this.canvas.height / height;
	}
	else {
		centerX = this.oldCenterX;
		centerY = this.oldCenterY;
	}

	// Effaçage du canvas
	this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

	// Translation du canvas
	this.context.save();
	this.context.translate(this.canvas.width / 2, this.canvas.height / 2);
	this.context.scale(this.scaleX, this.scaleY);
	this.context.translate(- centerX, - centerY);

	// Affichage du quadrillage
	var halfWidth = this.width / 2;
	var halfHeight = this.height / 2;
	this.context.strokeStyle = '#cb0020';
	this.context.globalAlpha = 0.3;
	this.context.lineWidth = 0.2;
	this.context.beginPath();
	for(i = Math.floor(-halfWidth + (halfWidth % 10)); i < halfWidth; i = i + 10) {
		this.context.moveTo(i, - halfHeight);
		this.context.lineTo(i, halfHeight);
	}
	for(i = Math.floor(-halfHeight + (halfHeight % 10)); i < halfHeight; i = i + 10) {
		this.context.moveTo(- halfWidth, i);
		this.context.lineTo(halfWidth, i);
	}
	this.context.stroke();

	// Affichage des cellules
	var size, measureText;
	this.context.lineWidth = 1;
	this.cells.sort(function(a, b) {
		if(a.mass < b.mass)
			return -1;
		if(a.mass > b.mass)
			return 1;
		return 0;
	});
	var left = centerX - width / 2,
		right = centerX + width / 2,
		top = centerY - height / 2,
		bottom = centerY + height / 2;
	var drawn = 0,
		notDrawn = 0;
	this.context.globalAlpha = 1;
	for(i = 0; i < this.cells.length; i++) {
		cell = this.cells[i];

		if(left - cell.nradius < cell.x && cell.x < right + cell.nradius &&
			top - cell.nradius < cell.y && cell.y < bottom + cell.nradius) {
			drawn++;
			this.context.beginPath();
			if(cell.isVirus) {
				var radius = Math.sqrt(cell.mass / Math.PI);
				var x, y;
				this.context.moveTo(cell.x + radius, cell.y);

				var maxVertices = 32;
				for(j = 1; j <= maxVertices; j++) {
					if(j % 2 === 0) {
						x = cell.x + radius * Math.cos(j * 2 * Math.PI / maxVertices);
						y = cell.y + radius * Math.sin(j * 2 * Math.PI / maxVertices);
					}
					else {
						x = cell.x + 1.2 * radius * Math.cos(j * 2 * Math.PI / maxVertices);
						y = cell.y + 1.2 * radius * Math.sin(j * 2 * Math.PI / maxVertices);
					}
					this.context.lineTo(x, y);
				}
			}
			else {
				this.context.arc(cell.x, cell.y, Math.sqrt(cell.mass / Math.PI), 0, 2 * Math.PI);
			}
			// this.context.globalAlpha = cell.alpha;
			this.context.fillStyle = cell.player.color;
			this.context.fill();
			if(cell.player.name !== '') {
				size = cell.radius() / 3;
				this.context.lineWidth = size / 4;
				this.context.font = size + "px monospace";
				this.context.strokeStyle = '#000';
				this.context.fillStyle = '#fff';
				measureText = this.context.measureText(cell.player.name);
				this.context.strokeText(cell.player.name, cell.x - measureText.width / 2,
					cell.y + size / 4);
				this.context.fillText(cell.player.name, cell.x - measureText.width / 2,
					cell.y + size / 4);
				this.context.lineWidth = 1;
			}
		}
		else {
			notDrawn++;
		}
	}
	this.context.restore();
	// console.log(drawn, notDrawn);

	// Demander l'afichage de la frame suivante
	// setTimeout(function() {
	// 	that.display();
	// }, 25);
	window.requestAnimationFrame(function() { that.display(); });
}

Game.prototype.onReceiveBuffer = function(buffer) {
	var dataView = new DataView(buffer);
	var c = 0;
	var header = dataView.getUint8(c++);

	if(header === 0) {
		this.id = dataView.getUint32(c); c += 4;
	}
	else if(header === 1) {
		this.width = dataView.getUint16(c); c += 2;
		this.height = dataView.getUint16(c); c += 2;
	}
	else if(header === 2) {
		this.leaders.length = 0;

		var nbrLeaders = dataView.getUint8(c); c += 1;
		for(var k = 0; k < nbrLeaders; k++) {
			this.leaders.push(dataView.getUint32(c)); c += 4;
		}


		// Affichage du leaderboard
		var that = this;
		var leaderboardContent = document.getElementById('leaderboard-content');
		while (leaderboardContent.firstChild)
			leaderboardContent.removeChild(leaderboardContent.firstChild);
		for(i = 0; i < this.leaders.length; i++) {
			var element = document.createElement('span');

			element.appendChild(document.createTextNode(this.playersById[this.leaders[i]].name || 'Une 7llule anonyme'));

			if(this.id === this.leaders[i])
				element.className = 'own';
			leaderboardContent.appendChild(element);
		}
	}
	else if(header === 4) {
		var id, name, charCode, color, player;

		this.players.length = 0;
		this.playersById = {};

		var nbrPlayers = dataView.getUint16(c); c += 2;
		for(var k = 0; k < nbrPlayers; k++) {
			id = dataView.getUint32(c); c += 4;
			name = '';
			charCode = dataView.getUint8(c); c += 1;
			while(charCode !== 0) {
				name += String.fromCharCode(charCode);
				charCode = dataView.getUint8(c); c += 1;
			}
			color = dataView.getUint32(c); c += 4;

			player = new Player(this, id, name, color);
			this.players.push(player);
			this.playersById[id] = player;
		}

		// Les joueurs ont été reçus
		this.playersReceived = true;
		if(this.playersReceived && this.cellsReceived) {
			this.interfaceInitialized = true;
			this._initInterface();
		}
	}
	else if(header === 5) {
		var id, name, charCode, color, player;

		var nbrCreated = dataView.getUint16(c); c += 2;
		for(var k = 0; k < nbrCreated; k++) {
			id = dataView.getUint32(c); c += 4;
			name = '';
			charCode = dataView.getUint8(c); c += 1;
			while(charCode !== 0) {
				name += String.fromCharCode(charCode);
				charCode = dataView.getUint8(c); c += 1;
			}
			color = dataView.getUint32(c); c += 4;

			if(!this.playersById[id]) {
				player = new Player(this, id, name, color);
				this.players.push(player);
				this.playersById[id] = player;
			}
		}

		var nbrModified = dataView.getUint16(c); c += 2;
		for(var k = 0; k < nbrModified; k++) {
			id = dataView.getUint32(c); c += 4;
			name = '';
			charCode = dataView.getUint8(c); c += 1;
			while(charCode !== 0) {
				name += String.fromCharCode(charCode);
				charCode = dataView.getUint8(c); c += 1;
			}

			if(this.playersById[id]) {
				player = this.playersById[id];
				player.update(name);
			}
		}

		var nbrDestroyed = dataView.getUint16(c); c += 2;
		for(var k = 0; k < nbrDestroyed; k++) {
			id = dataView.getUint32(c); c += 4;

			player = this.playersById[id];
			if(player) {
				this.players.splice(this.players.indexOf(player), 1);
				delete this.playersById[id];
			}
		}
	}
	else if(header === 6) {
		var id, playersId, player, x, y, mass, isVirus, cell;

		this.cells.length = 0;
		this.cellsById = {};

		var nbrCells = dataView.getUint16(c); c += 2;
		for(var k = 0; k < nbrCells; k++) {
			id = dataView.getUint32(c); c += 4;
			playersId = dataView.getUint32(c); c += 4;
			if(playersId !== 0)
				player = this.playersById[playersId];
			else
				player = null;
			x = dataView.getFloat32(c); c += 4;
			y = dataView.getFloat32(c); c += 4;
			mass = dataView.getUint16(c); c += 2;
			isVirus = dataView.getUint8(c) === 1 ? true : false; c += 1;

			cell = new Cell(this, id, player, x, y, mass, isVirus);
			this.cells.push(cell);
			this.cellsById[id] = cell;
			if(playersId === this.id)
				this.myCells.push(cell);
		}

		// Les cellues ont été reçues
		this.cellsReceived = true;
		if(this.playersReceived && this.cellsReceived) {
			this.interfaceInitialized = true;
			this._initInterface();
		}
	}
	else if(header === 7) {
		var id, playersId, player, x, y, mass, isVirus;

		var nbrCreated = dataView.getUint16(c); c += 2;
		for(var k = 0; k < nbrCreated; k++) {
			id = dataView.getUint32(c); c += 4;
			playersId = dataView.getUint32(c); c += 4;
			if(playersId !== 0) {
				player = this.playersById[playersId];
			}
			else
				player = null;
			x = dataView.getFloat32(c); c += 4;
			y = dataView.getFloat32(c); c += 4;
			mass = dataView.getUint16(c); c += 2;
			isVirus = dataView.getUint8(c) === 1 ? true : false; c += 1;

			if(!this.cellsById[id]) {
				cell = new Cell(this, id, player, x, y, mass, isVirus);
				this.cells.push(cell);
				this.cellsById[id] = cell;
				if(playersId === this.id)
					this.myCells.push(cell);
			}
		}

		var nbrModified = dataView.getUint16(c); c += 2;
		for(var k = 0; k < nbrModified; k++) {
			id = dataView.getUint32(c); c += 4;
			x = dataView.getFloat32(c); c += 4;
			y = dataView.getFloat32(c); c += 4;
			mass = dataView.getUint16(c); c += 2;

			cell = this.cellsById[id];
			if(cell) {
				cell.update(x, y, mass);
			}
		}

		var nbrDestroyed = dataView.getUint16(c); c += 2;
		for(var k = 0; k < nbrDestroyed; k++) {
			id = dataView.getUint32(c); c += 4;
			x = dataView.getFloat32(c); c += 4;
			y = dataView.getFloat32(c); c += 4;
			mass = dataView.getUint16(c); c += 2;

			cell = this.cellsById[id];
			if(cell) {
				cell.die(x, y, mass);
			}
		}
	}
	else if(header === 8) {
		c = this._onReceiveBase(dataView, c);
	}
	else if(header === 9) {
		c = this._onReceiveBase(dataView, c);
		c = this._onReceiveLeaderboard(dataView, c);
	}
	else if(header === 10) {
		this.showMenu();
		this.isPlaying = false;
	}
}

Game.prototype._onReceiveBase = function(dataView, c) {
	var id, name, charCode, color, player, playersId, x, y, mass, isVirus;

	var nbrCreated = dataView.getUint16(c); c += 2;

	for(var k = 0; k < nbrCreated; k++) {
		id = dataView.getUint32(c); c += 4;
		name = '';
		charCode = dataView.getUint8(c); c += 1;
		while(charCode !== 0) {
			name += String.fromCharCode(charCode);
			charCode = dataView.getUint8(c); c += 1;
		}
		color = dataView.getUint32(c); c += 4;

		if(!this.playersById[id]) {
			player = new Player(this, id, name, color);
			this.players.push(player);
			this.playersById[id] = player;
		}
	}

	var nbrModified = dataView.getUint16(c); c += 2;
	for(var k = 0; k < nbrModified; k++) {
		id = dataView.getUint32(c); c += 4;
		name = '';
		charCode = dataView.getUint8(c); c += 1;
		while(charCode !== 0) {
			name += String.fromCharCode(charCode);
			charCode = dataView.getUint8(c); c += 1;
		}

		if(this.playersById[id]) {
			player = this.playersById[id];
			player.update(name);
		}
	}

	var nbrDestroyed = dataView.getUint16(c); c += 2;
	for(var k = 0; k < nbrDestroyed; k++) {
		id = dataView.getUint32(c); c += 4;

		player = this.playersById[id];
		if(player) {
			this.players.splice(this.players.indexOf(player), 1);
			delete this.playersById[id];
		}
	}

	var nbrCreatedB = dataView.getUint16(c); c += 2;
	for(var k = 0; k < nbrCreatedB; k++) {
		id = dataView.getUint32(c); c += 4;
		playersId = dataView.getUint32(c); c += 4;
		if(playersId !== 0) {
			player = this.playersById[playersId];
		}
		else
			player = null;
		x = dataView.getFloat32(c); c += 4;
		y = dataView.getFloat32(c); c += 4;
		mass = dataView.getUint16(c); c += 2;
		isVirus = dataView.getUint8(c) === 1 ? true : false; c += 1;

		if(!this.cellsById[id]) {
			cell = new Cell(this, id, player, x, y, mass, isVirus);
			this.cells.push(cell);
			this.cellsById[id] = cell;
			if(playersId === this.id)
				this.myCells.push(cell);
		}
	}

	var nbrModifiedB = dataView.getUint16(c); c += 2;
	for(var k = 0; k < nbrModifiedB; k++) {
		id = dataView.getUint32(c); c += 4;
		x = dataView.getFloat32(c); c += 4;
		y = dataView.getFloat32(c); c += 4;
		mass = dataView.getUint16(c); c += 2;

		cell = this.cellsById[id];
		if(cell) {
			cell.update(x, y, mass);
		}
	}

	var nbrDestroyedB = dataView.getUint16(c); c += 2;
	for(var k = 0; k < nbrDestroyedB; k++) {
		id = dataView.getUint32(c); c += 4;
		x = dataView.getFloat32(c); c += 4;
		y = dataView.getFloat32(c); c += 4;
		mass = dataView.getUint16(c); c += 2;

		cell = this.cellsById[id];
		if(cell) {
			cell.die(x, y, mass);
		}
	}

	return c;
};

Game.prototype._onReceiveLeaderboard = function(dataView, c) {
	this.leaders.length = 0;

	var nbrLeaders = dataView.getUint8(c); c += 1;
	for(var k = 0; k < nbrLeaders; k++) {
		this.leaders.push(dataView.getUint32(c)); c += 4;
	}


	// Affichage du leaderboard
	var that = this;
	var leaderboardContent = document.getElementById('leaderboard-content');
	while (leaderboardContent.firstChild)
		leaderboardContent.removeChild(leaderboardContent.firstChild);
	for(i = 0; i < this.leaders.length; i++) {
		var element = document.createElement('span');

		element.appendChild(document.createTextNode(this.playersById[this.leaders[i]].name || 'Une 7llule anonyme'));

		if(this.id === this.leaders[i])
			element.className = 'own';
		leaderboardContent.appendChild(element);
	}

	return c;
};

Game.prototype.updateMouseDirection = function(x, y) {
	var time = new Date();
	var delta = time - this.sendTime;
	if(this.isPlaying && delta > 30) {
		this.sendTime = time;

		var buffer = new ArrayBuffer(9);
		var dataView = new DataView(buffer);
		dataView.setUint8(0, 16);
		dataView.setInt32(1, Math.round(x * 100));
		dataView.setInt32(5, Math.round(y * 100));
		this.webSocket.send(dataView.buffer);
	}
	else {
		var that = this;
		if(this.sendTimeout)
			clearTimeout(this.sendTimeout);
		this.sendTimeout = setTimeout(function() {
			that.updateMouseDirection(x, y);
		}, 30 - delta);
	}
}

Game.prototype.feed = function() {
	var time = new Date();
	var delta = time - this.sendTime;
	this.sendTime = time;
	if(this.isPlaying && delta > 10) {
		var buffer = new ArrayBuffer(1);
		var dataView = new DataView(buffer);
		dataView.setUint8(0, 18);
		this.webSocket.send(dataView.buffer);
	}
}

Game.prototype.split = function() {
	var time = new Date();
	var delta = time - this.sendTime;
	this.sendTime = time;
	if(this.isPlaying && delta > 10) {
		var buffer = new ArrayBuffer(1);
		var dataView = new DataView(buffer);
		dataView.setUint8(0, 17);
		this.webSocket.send(dataView.buffer);
	}
}









function Cell(game, id, player, x, y, mass, isVirus) {
	this.game = game;
	this.id = id;
	this.player = player || {
		name: '',
		color: isVirus ? '#00cb00' : '#'+Math.floor(Math.random()*16777215).toString(16)
	};
	this.nx = x;
	this.ny = y;
	this.x = x;
	this.y = y;
	this.ox = x;
	this.oy = y;
	this.nmass = mass;
	this.mass = mass;
	this.nradius = Math.sqrt(mass / Math.PI);
	this.omass = mass;
	this.isVirus = isVirus;
	this.isAlive = true;
	this.nalpha = 1;
	this.alpha = 1;
	this.oalpha = 1;
	this.updateTime = new Date();
}

Cell.prototype.update = function(x, y, mass) {
	this.updateTime = new Date();
	this.ox = this.x;
	this.oy = this.y;
	this.nx = x;
	this.ny = y;
	this.omass = this.mass;
	this.nmass = mass;
	this.nradius = Math.sqrt(mass / Math.PI);
};

Cell.prototype.die = function(x, y, mass) {
	this.updateTime = new Date();
	this.ox = this.x;
	this.oy = this.y;
	this.nx = x;
	this.ny = y;
	this.omass = this.mass;
	this.nmass = mass;
	this.nradius = Math.sqrt(mass / Math.PI);
	this.nalpha = 0;
	this.isAlive = false;
	this.deathTime = new Date();
};
Cell.prototype.undie = function(x, y, mass) {
	this.updateTime = new Date();
	this.ox = this.x;
	this.oy = this.y;
	this.nx = x;
	this.ny = y;
	this.omass = this.mass;
	this.nmass = mass;
	this.nradius = Math.sqrt(mass / Math.PI);
	this.nalpha = 1;
	this.isAlive = true;
	this.deathTime = null;
}

Cell.prototype.refresh = function(time) {
	var timeRatio = Math.min(1, (time - this.updateTime) / this.game.latency);
	this.x = (this.nx - this.ox) * timeRatio + this.ox;
	this.y = (this.ny - this.oy) * timeRatio + this.oy;
	this.mass = (this.nmass - this.omass) * timeRatio + this.omass;
	this.alpha = (this.nalpha - this.oalpha) * timeRatio + this.oalpha;
};

Cell.prototype.timeSinceDeath = function() {
	return new Date() - this.deathTime;
}

Cell.prototype.radius = function() {
	return Math.sqrt(this.mass / Math.PI);
}

function Player(game, id, name, color) {
	this.game = game;
	this.id = id;
	this.name = name;
	this.color = '#'+color.toString(16);
}

Player.prototype.update = function(name) {
	this.name = name;
}