var DataView = require('buffer-dataview');
var Game = require('./Game.js');
var Cell = require('./Cell.js');
var Vector = require('./Vector.js');

function Observer(game, ws) {
	this._game = game;
	this._ws = ws;
	this._isObserving = true;

	this._clientKnownCells = [];
	this._nbrPacketsSent = 0;
}

Observer.prototype.stopObserving = function() {
	this._isObserving = false;
};

Observer.prototype.send = function() {
	if(this._isObserving) {
		var destroyed = [],
			created = [],
			modified = [],
			cell, i, j, position, 
			gameCells = this._game.cells,
			nameLength, color;
		var nbrGameCells = gameCells.length;

		// Cellules détruites et modifiées
		for(i = 0; i < this._clientKnownCells.length; i++) {
			cell = this._clientKnownCells[i];

			if(!cell.isAlive()) {
				destroyed.push(cell);
				this._clientKnownCells.splice(i--, 1);
			}
			else if(cell.hasBeenModified()) {
				modified.push(cell);
			}
		}

		// Cellules crées
		for(var i = 0; i < nbrGameCells; i++) {
			cell = gameCells[i];
			if(this._clientKnownCells.indexOf(cell) === -1 && cell.isAlive()) {
				created.push(cell);
				this._clientKnownCells.push(cell);
			}
		}

		var bufferLength = 0;
		var nbrDestroyed = destroyed.length,
			nbrCreated = created.length,
			nbrModified = modified.length;
		
		// Calcul du nombre de caractères à envoyer (noms des joueurs)
		var nbrChars = 0;
		for(i = 0; i < nbrDestroyed; i++) {
			if(destroyed[i].player() !== null)
				nbrChars += destroyed[i].player().name().length + 1;
			else
				nbrChars += 1;
		}
		for(i = 0; i < nbrCreated; i++) {
			if(created[i].player() !== null)
				nbrChars += created[i].player().name().length + 1;
			else
				nbrChars += 1;
		}
		for(i = 0; i < nbrModified; i++) {
			if(modified[i].player() !== null)
				nbrChars += modified[i].player().name().length + 1;
			else
				nbrChars += 1;
		}

		if(nbrDestroyed !== 0 || nbrCreated !== 0 || nbrModified !== 0) {
			// Calcul de la taille du buffer à prévoir
			bufferLength = 1 + 2 + 6 + (nbrDestroyed + nbrCreated + nbrModified) * 22 + nbrChars;
			var dataView = new DataView(new Buffer(bufferLength));
			var c = 0;
			dataView.setUint8(c++, 7);
			dataView.setUint16(c, ++this._nbrPacketsSent); c += 2;

			// Ajout dans le buffer des cellules détruites
			dataView.setUint16(c, nbrDestroyed); c += 2;
			for(i = 0; i < nbrDestroyed; i++) {
				cell = destroyed[i];
				position = cell.position();
				player = cell.player()

				// id
				dataView.setUint32(c, cell.id()); c += 4;
				// player's id
				dataView.setUint32(c, player ? player.id() : 0); c += 4;
				// x
				dataView.setFloat32(c, position.x()); c += 4;
				// y
				dataView.setFloat32(c, position.y()); c += 4;
				// mass
				dataView.setUint16(c, cell.mass()); c += 2;
				// name
				name = (player !== null) ? player.name() : '';
				nameLength = name.length
				for(j = 0; j < nameLength; j++)
					dataView.setUint8(c++, name.charCodeAt(j));
				dataView.setUint8(c++, 0);
				// color && isVirus && isMine
				color = cell.color() << 7;
				if(cell.isVirus())
					color += 1;
				else if(this === cell.player())
					color += 2;
				dataView.setUint32(c, color); c += 4;
			}
			// Ajout dans le buffer des cellules crées
			dataView.setUint16(c, nbrCreated); c += 2;
			for(i = 0; i < nbrCreated; i++) {
				cell = created[i];
				position = cell.position();
				player = cell.player()

				// id
				dataView.setUint32(c, cell.id()); c += 4;
				// player's id
				dataView.setUint32(c, player ? player.id() : 0); c += 4;
				// x
				dataView.setFloat32(c, position.x()); c += 4;
				// y
				dataView.setFloat32(c, position.y()); c += 4;
				// mass
				dataView.setUint16(c, cell.mass()); c += 2;
				// name
				name = (player !== null) ? player.name() : '';
				nameLength = name.length
				for(j = 0; j < nameLength; j++)
					dataView.setUint8(c++, name.charCodeAt(j));
				dataView.setUint8(c++, 0);
				// color && isVirus && isMine
				color = cell.color();
				color = color << 7;
				if(cell.isVirus())
					color += 1;
				else if(this === cell.player())
					color += 2;
				dataView.setUint32(c, color); c += 4;
			}
			// Ajout dans le buffer des cellules modifiées
			dataView.setUint16(c, nbrModified); c += 2;
			for(i = 0; i < nbrModified; i++) {
				cell = modified[i];
				position = cell.position();
				player = cell.player()

				// id
				dataView.setUint32(c, cell.id()); c += 4;
				// player's id
				dataView.setUint32(c, player ? player.id() : 0); c += 4;
				// x
				dataView.setFloat32(c, position.x()); c += 4;
				// y
				dataView.setFloat32(c, position.y()); c += 4;
				// mass
				dataView.setUint16(c, cell.mass()); c += 2;
				// name
				name = (player !== null) ? player.name() : '';
				nameLength = name.length
				for(j = 0; j < nameLength; j++)
					dataView.setUint8(c++, name.charCodeAt(j));
				dataView.setUint8(c++, 0);
				// color && isVirus && isMine
				color = cell.color() << 7;
				if(cell.isVirus())
					color += 1;
				else if(this === cell.player())
					color += 2;
				dataView.setUint32(c, color%4294967296); c += 4;
			}

			// Envoi du buffer
			this._ws.send(dataView.buffer, function ack(error) {
				if(error)
					console.log('Socket Error');
			});
		}
	}
};

Observer.prototype.sendGameParams = function() {
	if(this._isObserving) {
		var bufferLength = 5;
		var dataView = new DataView(new Buffer(bufferLength));
		dataView.setUint8(0, 0);
		dataView.setUint16(1, this._game.width());
		dataView.setUint16(3, this._game.height());

		// Envoi du buffer
		this._ws.send(dataView.buffer, function ack(error) {
			if(error)
				console.log('Socket Error');
		});
	}
};

Observer.prototype.sendLeaderboard = function() {
	if(this._isObserving) {
		var leaders = this._game.leaders();

		// Calcul du nombre de caractères à envoyer (noms des joueurs)
		var nbrChars = 0, i, j;
		for(i = 0; i < leaders.length; i++) {
			nbrChars += leaders[i].name().length + 1;
		}

		// Préparation du buffer
		var bufferLength = 1 + 1 + leaders.length * 4 + nbrChars;
		var dataView = new DataView(new Buffer(bufferLength));
		var c = 0;

		dataView.setUint8(c++, 1);
		dataView.setUint8(c++, leaders.length);
		var name, nameLength
		for(i = 0; i < leaders.length; i++) {
			dataView.setUint32(c, leaders[i].id()); c += 4;

			name = leaders[i].name();
			nameLength = name.length
			for(j = 0; j < nameLength; j++)
				dataView.setUint8(c++, name.charCodeAt(j));
			dataView.setUint8(c++, 0);
		}

		// Envoi du buffer
		this._ws.send(dataView.buffer, function ack(error) {
			if(error)
				console.log('Socket Error');
		});
	}
};

module.exports = Observer;