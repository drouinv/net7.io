var DataView = require('buffer-dataview');
var Player = require('./Player.js');
var Cell = require('./Cell.js');
var Vector = require('./Vector.js');
var Observer = require('./Observer.js');

function Game() {
	this.cells = [];
	this.players = [];
	this._leaders = [];
	this._knownCells = [];
	this._knownPlayers = [];

	this.width = 89;
	this._expectedWidth = 89;
	this.height = 50;
	this._expectedHeight = 50;
	this._growing = null;

	this._time;
	this._nbrFrame;

	this._playerId = 1;
	this._cellId = 1;
};

Game.prototype.generatePlayerId = function() {

	return this._playerId++;
};

Game.prototype.generateCellId = function() {

	return this._cellId++;
};

Game.prototype.addCell = function(cell) {

	this.cells.push(cell);
};

Game.prototype.removeCell = function(cell) {
	var i = this.cells.indexOf(cell);
	if(i !== -1)
		this.cells.splice(i);
};

Game.prototype.addPlayer = function(player) {
	this.players.push(player);
};

Game.prototype.removePlayer = function(player) {
	var i = this.players.indexOf(player);
	if(i !== -1)
		this.players.splice(i);
};

Game.prototype.countConnectedPlayers = function() {
	var c = 0;
	for(var k = 0; k < this.players.length; k++) {
		if(this.players[k].isConnected) {
			c++;
		}
	}
	return c;
}

Game.prototype.countPlayingPlayers = function() {
	var c = 0;
	for(var k = 0; k < this.players.length; k++) {
		if(this.players[k].isPlaying) {
			c++;
		}
	}
	return c;
};

Game.prototype.countAlivePlayers = function() {
	var c = 0;
	for(var k = 0; k < this.players.length; k++) {
		if(this.players[k].isAlive) {
			c++;
		}
	}
	return c;
};

Game.prototype.createPlayer = function(ws) {
	var player = new Player(this, ws);

	// Ajouter le joueur au jeu en tant qu'observateur
	this.addPlayer(player);

	// Envoyer les paramètres du joueur à l'observateur
	player.send(player.infosBuffer());

	// Envoyer les paramètres du jeu à l'observateur
	player.send(this.paramsBuffer());

	// Envoyer les joueurs du jeu à l'observateur
	player.send(this.playersBuffer());

	// Envoyer les cellules du jeu à l'observateur
	player.send(this.cellsBuffer());

	return player;
};

Game.prototype.destroyPlayer = function(player) {
	this.players.splice(this.players.indexOf(player), 1);
	// Mettre à jour la taille attendue de la map
	this.updateExpectedSize();
};

Game.prototype.createPlayerFirstCell = function(player) {
	var cell = new Cell(this, player, new Vector(0, 0), 16, new Vector(0, 0), false);

	var ok = false;

	while(!ok) {
		ok = true;
		cell.setPosition(new Vector((Math.random() - 0.5) * this.width,
				(Math.random() - 0.5) * this.height));

		for(var k = 0; k < this.cells.length; k++) {
			if(this.cells[k].canEat(cell)) {
				ok = false;
				break;
			}
		}
	}
	// Ajouter la cellule au jeu
	this.addCell(cell);

	// Ajouter la cellule au joueur
	player.addCell(cell);

	// Maj de la taille du jeu
	this.updateExpectedSize();
};

Game.prototype.updateExpectedSize = function() {
	var factor = Math.sqrt(this.players.length);
	this._expectedWidth = Math.floor(89 + factor * 89);
	this._expectedHeight = Math.floor(50 + factor * 50);
};

Game.prototype.maxCellsNumber = function() {

	return this.players.length * 24;
};

Game.prototype.maxVirusNumber = function() {

	return Math.floor(this.players.length * 0.5);
}

Game.prototype.exec = function() {
	// Initialisation de la partie
	this._init();

	// Lancement de la boucle principale
	var that = this;
	this.frameInterval = setInterval(function() {
		that.frame();
	}, 30);
};

Game.prototype._init = function() {
	this._time = new Date();
	this._nbrFrame = 0;
};

Game.prototype.frame = function() {
	// Initialiser la frame
	this._nbrFrame = this._nbrFrame + 1;
	var newTime = new Date();
	var deltaTime = (newTime - this._time) / 1000;
	this._time = newTime;
	var nbrCells = this.cells.length,
		nbrPlayers = this.players.length;
	var i, j, k, vector, cells, cell, cellB, player, nbrPlayerCells, buffer;

	// Mettre à jour la taille du jeu
	if(this._nbrFrame % 2 === 1) {
		if(this.width !== this._expectedWidth || this.height !== this._expectedHeight) {
			if(this.width < this._expectedWidth)
				this.width = Math.min(this._expectedWidth, this.width + 16/9);
			else if(this.width > this._expectedWidth)
				this.width = Math.max(this._expectedWidth, this.width - 16/9);
			if(this.height < this._expectedHeight)
				this.height = Math.min(this._expectedHeight, this.height + 1);
			else if(this.height > this._expectedHeight)
				this.height = Math.max(this._expectedHeight, this.height - 1);

			// Replacer les cellules
			for(i = 0; i < nbrCells; i++) {
				this.cells[i].moveInsideLimits();
			}

			// Envoyer l'information aux clients
			buffer = this.paramsBuffer();
			for(i = 0; i < nbrPlayers; i++) {
				this.players[i].send(buffer);
			}
		}
	}

	// Jouer les joueurs
	for(i = 0; i < nbrPlayers; i++) {
		this.players[i].frame(deltaTime);
	}

	// Jouer les cellules
	for(i = 0; i < nbrCells; i++) {
		this.cells[i].frame(deltaTime);
	}

	// Calculer qui mange qui
	for(i = 0; i < nbrCells; i++) {
		cell = this.cells[i];

		if(cell.player !== null || cell.isVirus()) {
			for(var j = 0; j < nbrCells; j++) {
				cellB = this.cells[j];

				cell.eat(cellB);
			}
		}
	}

	// Diminuer la masse des grosse cellules
	if(this._nbrFrame % 166 === 0) {
		var mass;
		for(i = 0; i < nbrCells; i++) {
			cell = this.cells[i];
			mass = cell.mass;
			if(mass > 32 && !cell.isVirus()) {
				cell.setMass(mass - Math.ceil(mass / 128));
			}
		}
	}

	// Envoi du leaderboard aux clients
	if(this._nbrFrame % 33 === 0) {
		// Ordonner les joueurs
		this.players.sort(function(a, b) {
			if(!a.isAlive && !b.isAlive)
				return 0;
			else if(!a.isAlive)
				return 1;
			else if(!b.isAlive)
				return -1;
			var massA = a.mass(), massB = b.mass();
			if(massA < massB)
				return 1;
			else if(massA < massB)
				return -1;
			else
				return 0;
		});
		var nbrLeader = Math.min(10, this.countAlivePlayers());
		this._leaders.length = 0;
		for(i = 0; i < nbrLeader; i++) {
			this._leaders.push(this.players[i]);
		}
	}

	// Envoi des données aux clients
	if(this._nbrFrame % 33 === 0) {
		buffer = this.baseLeaderboardBuffer();
	}
	else {
		buffer = this.baseBuffer();
	}
	for(i = 0; i < nbrPlayers; i++) {
		this.players[i].send(buffer);
	}

	// Suppression des cellules mortes
	// & Marquer les cellules comme "non modifiées"
	for(i = 0; i < this.cells.length; i++) {
		if(!this.cells[i].isAlive) {
			this.cells.splice(i--, 1);
		}
		else {
			this.cells[i].setHasBeenModified(false);
		}
	}

	// Suppression des joueurs
	for(i = 0; i < this.players.length; i++) {
		if(!this.players[i].isAlive && !this.players[i].isPlaying && !this.players[i].isConnected) {
			this.destroyPlayer(this.players[i]);
			i--;
		}
	}

	// Compter les virus
	var nbrVirus = 0;
	nbrCells = this.cells.length;
	for(i = 0; i < nbrCells; i++) {
		if(this.cells[i].isVirus()) {
			nbrVirus++;
		}
	}

	// Créer un virus (~/1sec)
	if(this._nbrFrame % 166 === 1 && nbrVirus < this.maxVirusNumber()) {
		// Créer le virus
		virus = new Cell(this, null,
			new Vector((Math.random() - 0.5) * this.width,
				(Math.random() - 0.5) * this.height),
			100, new Vector(0, 0), true);
		virusRadius = virus.radius();
		
		// N'ajouter le virus que si il ne tue persone
		var ok = true;
		for(j = 0; j < nbrCells; j++) {
			if(this.cells[j].player !== null &&
				this.cells[j].radius() + 2 * virusRadius > this.cells[j].distance(virus)) {
				ok = false;
				break;
			}
		}
		if(ok) {
			this.addCell(virus);
			nbrVirus++;
		}
	}

	// Remplir le jeu avec des pellets (~/5sec)
	if(this._nbrFrame % 33 === 0) {
		var nbrIterations;
		var nbrPelletsToCreate = (this.maxCellsNumber() - nbrCells) / 2;
		var ok, cellRadius;
		var red, green, blue, tmp, color;
		nbrCells = this.cells.length;
		for(i = 0; i < nbrPelletsToCreate; i++) {
			red = Math.random() * 256;
			green = Math.random() * 256;
			blue = Math.random() * 256;
			tmp = Math.floor(Math.random() * 3);
			if(tmp === 0)
				red = Math.max(0, 2 * red - green - blue);
			else if(tmp === 1)
				green = Math.max(0, 2 * green - red - blue);
			else
				blue = Math.max(0, 2 * blue - red - green);
			color = red * 65536 + green * 256 + blue;
			cell = new Cell(this, null, new Vector(0, 0), 1);
			cellRadius = cell.radius();
			nbrIterations = 0;
			do {
				nbrIterations++;
				ok = true;
				cell.position.setX((Math.random() - 0.5) * this.width);
				cell.position.setY((Math.random() - 0.5) * this.height);
				for(j = 0; j < nbrCells; j++) {
					if(this.cells[j].mass !== 1 &&
						this.cells[j].radius() + 2 * cellRadius > this.cells[j].distance(cell)) {
						ok = false;
						break;
					}
				}
			} while(!ok && nbrIterations < 1000);
			this.addCell(cell);
		}
	}

	// Log latence + FPS
	if(this._nbrFrame % 10 === 0) {
		console.log('Latence : ' + (deltaTime * 1000) + 'ms, FPS : ' + Math.round(1 / deltaTime) + ', NoC : ' + this.cells.length + '(/' + this.maxCellsNumber() + ')' + ', NoV : ' + nbrVirus + '(/' + this.maxVirusNumber() + ')' + ', NoP : ' + this.players.length);
	}
};

// 1
Game.prototype.paramsBuffer = function() {
	var bufferLength = 5,
		dataView;
	dataView = new DataView(new Buffer(bufferLength));
	dataView.setUint8(0, 1);
	dataView.setUint16(1, this.width);
	dataView.setUint16(3, this.height);

	return dataView.buffer;
};

// 2
Game.prototype.leaderboardBuffer = function() {
	var bufferLength,
		dataView,
		k,
		c = 0;

	// Préparation du buffer
	bufferLength = 1 + 1 + this._leaders.length * 4;
	dataView = new DataView(new Buffer(bufferLength));
	dataView.setUint8(c, 2); c += 1;

	// Ajout au buffer des joueurs
	dataView.setUint8(c, this._leaders.length); c += 1;
	for(k = 0; k < this._leaders.length; k++) {
		dataView.setUint32(c, this._leaders[k].id); c += 4;
	}

	return dataView.buffer;
};

// 4
Game.prototype.playersBuffer = function() {
	var created = [],
		c = 0,
		nbrChars = 0,
		player,
		k, i,
		dataView,
		bufferLength;
	var nbrPlayers = this.players.length;

	// Décompte des caractères
	for(k = 0; k < nbrPlayers; k++) {
		nbrChars += this.players[k].name.length + 1;
	}

	// Création du buffer
	bufferLength = 1 + 2 + nbrPlayers * 8 + nbrChars;
	dataView = new DataView(new Buffer(bufferLength));
	dataView.setUint8(c, 4); c += 1;

	// Ajout dans le buffer des joueurs
	dataView.setUint16(c, nbrPlayers); c += 2;
	for(k = 0; k < nbrPlayers; k++) {
		player = this.players[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
		// name
		for(i = 0; i < player.name.length; i++) {
			dataView.setUint8(c, player.name.charCodeAt(i)); c += 1;
		}
		dataView.setUint8(c, 0); c += 1;
		// color
		dataView.setUint32(c, player.color); c += 4;
	}

	return dataView.buffer;
};

// 5
Game.prototype.playersDiffBuffer = function() {
	var created = [],
		modified = [],
		destroyed = [],
		c = 0,
		nbrChars = 0,
		player,
		k,
		dataView,
		bufferLength;
	var nbrPlayers = this.players.length;

	// Joueurs créés
	for(k = 0; k < nbrPlayers; k++) {
		player = this.players[k];

		if(this._knownPlayers.indexOf(player) === -1) {
			created.push(player);
			this._knownPlayers.push(player);
			nbrChars += player.name.length + 1;
		}
	}

	// Joueurs modifiés et détruits
	for(k = 0; k < this._knownPlayers.length; k++) {
		player = this._knownPlayers[k];

		if(!player.isConnected && !player.isPlaying && !player.isAlive) {
			destroyed.push(player);
			this._knownPlayers.splice(k--, 1);
		}
		else if(player.hasBeenModified) {
			modified.push(player);
			player.unmodify();
			nbrChars += player.name.length + 1;
		}
	}

	// Création du buffer
	bufferLength = 1 + 6 + created.length * 8 + modified.length * 4 + destroyed.length * 4 + nbrChars;
	dataView = new DataView(new Buffer(bufferLength));
	dataView.setUint8(c, 5); c += 1;

	// Ajout dans le bufffer des joueurs créés
	dataView.setUint16(c, created.length); c += 2;
	for(k = 0; k < created.length; k++) {
		player = created[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
		// name
		for(i = 0; i < player.name.length; i++) {
			dataView.setUint8(c, player.name.charCodeAt(i)); c += 1;
		}
		dataView.setUint8(c, 0); c += 1;
		// color
		dataView.setUint32(c, player.color); c += 4;
	}

	// Ajout dans le bufffer des joueurs modifiés
	dataView.setUint16(c, modified.length); c += 2;
	for(k = 0; k < modified.length; k++) {
		player = modified[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
		// name
		for(i = 0; i < player.name.length; i++) {
			dataView.setUint8(c, player.name.charCodeAt(i)); c += 1;
		}
		dataView.setUint8(c, 0); c += 1;
	}

	// Ajout dans le bufffer des joueurs détruits
	dataView.setUint16(c, destroyed.length); c += 2;
	for(k = 0; k < destroyed.length; k++) {
		player = destroyed[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
	}

	return dataView.buffer;
};

// 6
Game.prototype.cellsBuffer = function() {
	var created = [],
		c = 0,
		nbrCells = this.cells.length,
		cell,
		k,
		position,
		player,
		color,
		dataView,
		bufferLength;
	var nbrCells = this.cells.length;

	// Création du buffer
	bufferLength = 1 + 2 + nbrCells * 19;
	dataView = new DataView(new Buffer(bufferLength));
	dataView.setUint8(c, 6); c += 1;

	// Ajout dans le buffer des cellules
	dataView.setUint16(c, nbrCells); c += 2;
	for(k = 0; k < nbrCells; k++) {
		cell = this.cells[k];
		position = cell.position;
		player = cell.player;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// player's id
		dataView.setUint32(c, player ? player.id : 0); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// mass
		dataView.setUint16(c, cell.mass); c += 2;
		// isVirus
		dataView.setUint8(c, cell.isVirus()); c += 1;
	}

	return dataView.buffer;
};

// 7
Game.prototype.cellsDiffBuffer = function() {
	var created = [],
		modified = [],
		destroyed = [],
		c = 0,
		cell,
		k,
		position,
		player,
		dataView,
		bufferLength;
	var nbrCells = this.cells.length;

	// Cellules créées
	for(k = 0; k < nbrCells; k++) {
		cell = this.cells[k];

		if(this._knownCells.indexOf(cell) === -1) {
			created.push(cell);
			this._knownCells.push(cell);
		}
	}

	// Cellules modifiées et détruites
	for(k = 0; k < this._knownCells.length; k++) {
		cell = this._knownCells[k];

		if(!cell.isAlive) {
			destroyed.push(cell);
			this._knownCells.splice(k--, 1);
		}
		else if(cell.hasBeenModified) {
			modified.push(cell);
		}
	}

	// Création du buffer
	bufferLength = 1 + 6 + created.length * 19 + modified.length * 14 + destroyed.length * 14;
	dataView = new DataView(new Buffer(bufferLength));
	dataView.setUint8(c, 7); c += 1;
	
	// Ajout dans le buffer des cellules créées
	dataView.setUint16(c, created.length); c += 2;
	for(k = 0; k < created.length; k++) {
		cell = created[k];
		position = cell.position;
		player = cell.player;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// player's id
		dataView.setUint32(c, player ? player.id : 0); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// mass
		dataView.setUint16(c, cell.mass); c += 2;
		// isVirus
		dataView.setUint8(c, cell.isVirus()); c += 1;
	}

	// Ajout dans le buffer des cellules modifiées
	dataView.setUint16(c, modified.length); c += 2;
	for(k = 0; k < modified.length; k++) {
		cell = modified[k];
		position = cell.position;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// x
		dataView.setUint16(c, cell.mass); c += 2;
	}

	// Ajout dans le buffer des cellules détruites
	dataView.setUint16(c, destroyed.length); c += 2;
	for(k = 0; k < destroyed.length; k++) {
		cell = destroyed[k];
		position = cell.position;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// x
		dataView.setUint16(c, cell.mass); c += 2;
	}

	return dataView.buffer;
};

// 8
Game.prototype.baseBuffer = function() {
	var created = [],
		modified = [],
		destroyed = [],
		c = 0,
		nbrChars = 0,
		player,
		k,
		dataView,
		bufferLength;
	var nbrPlayers = this.players.length;

	var createdB = [],
		modifiedB = [],
		destroyedB = [],
		cell,
		position;
	var nbrCells = this.cells.length;

	// Joueurs créés
	for(k = 0; k < nbrPlayers; k++) {
		player = this.players[k];

		if(this._knownPlayers.indexOf(player) === -1) {
			created.push(player);
			this._knownPlayers.push(player);
			nbrChars += player.name.length + 1;
		}
	}

	// Joueurs modifiés et détruits
	for(k = 0; k < this._knownPlayers.length; k++) {
		player = this._knownPlayers[k];

		if(!player.isConnected && !player.isPlaying && !player.isAlive) {
			destroyed.push(player);
			this._knownPlayers.splice(k--, 1);
		}
		else if(player.hasBeenModified) {
			modified.push(player);
			player.unmodify();
			nbrChars += player.name.length + 1;
		}
	}

	// Cellules créées
	for(k = 0; k < nbrCells; k++) {
		cell = this.cells[k];

		if(this._knownCells.indexOf(cell) === -1) {
			createdB.push(cell);
			this._knownCells.push(cell);
		}
	}

	// Cellules modifiées et détruites
	for(k = 0; k < this._knownCells.length; k++) {
		cell = this._knownCells[k];

		if(!cell.isAlive) {
			destroyedB.push(cell);
			this._knownCells.splice(k--, 1);
		}
		else if(cell.hasBeenModified) {
			modifiedB.push(cell);
		}
	}

	// Création du buffer
	bufferLength = 1 + 6 + created.length * 8 + modified.length * 4 + destroyed.length * 4 + nbrChars
		+ 6 + createdB.length * 19 + modifiedB.length * 14 + destroyedB.length * 14;
	dataView = new DataView(new Buffer(bufferLength));
	dataView.setUint8(c, 8); c += 1;

	// Ajout dans le bufffer des joueurs créés
	dataView.setUint16(c, created.length); c += 2;
	for(k = 0; k < created.length; k++) {
		player = created[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
		// name
		for(i = 0; i < player.name.length; i++) {
			dataView.setUint8(c, player.name.charCodeAt(i)); c += 1;
		}
		dataView.setUint8(c, 0); c += 1;
		// color
		dataView.setUint32(c, player.color); c += 4;
	}

	// Ajout dans le bufffer des joueurs modifiés
	dataView.setUint16(c, modified.length); c += 2;
	for(k = 0; k < modified.length; k++) {
		player = modified[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
		// name
		for(i = 0; i < player.name.length; i++) {
			dataView.setUint8(c, player.name.charCodeAt(i)); c += 1;
		}
		dataView.setUint8(c, 0); c += 1;
	}

	// Ajout dans le bufffer des joueurs détruits
	dataView.setUint16(c, destroyed.length); c += 2;
	for(k = 0; k < destroyed.length; k++) {
		player = destroyed[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
	}
	
	// Ajout dans le buffer des cellules créées
	dataView.setUint16(c, createdB.length); c += 2;
	for(k = 0; k < createdB.length; k++) {
		cell = createdB[k];
		position = cell.position;
		player = cell.player;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// player's id
		dataView.setUint32(c, player ? player.id : 0); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// mass
		dataView.setUint16(c, cell.mass); c += 2;
		// isVirus
		dataView.setUint8(c, cell.isVirus()); c += 1;
	}

	// Ajout dans le buffer des cellules modifiées
	dataView.setUint16(c, modifiedB.length); c += 2;
	for(k = 0; k < modifiedB.length; k++) {
		cell = modifiedB[k];
		position = cell.position;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// x
		dataView.setUint16(c, cell.mass); c += 2;
	}

	// Ajout dans le buffer des cellules détruites
	dataView.setUint16(c, destroyedB.length); c += 2;
	for(k = 0; k < destroyedB.length; k++) {
		cell = destroyedB[k];
		position = cell.position;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// x
		dataView.setUint16(c, cell.mass); c += 2;
	}

	return dataView.buffer;
};

// 9
Game.prototype.baseLeaderboardBuffer = function() {
	var created = [],
		modified = [],
		destroyed = [],
		c = 0,
		nbrChars = 0,
		player,
		k,
		dataView,
		bufferLength;
	var nbrPlayers = this.players.length;

	var createdB = [],
		modifiedB = [],
		destroyedB = [],
		cell,
		position;
	var nbrCells = this.cells.length;

	// Joueurs créés
	for(k = 0; k < nbrPlayers; k++) {
		player = this.players[k];

		if(this._knownPlayers.indexOf(player) === -1) {
			created.push(player);
			this._knownPlayers.push(player);
			nbrChars += player.name.length + 1;
		}
	}

	// Joueurs modifiés et détruits
	for(k = 0; k < this._knownPlayers.length; k++) {
		player = this._knownPlayers[k];

		if(!player.isConnected && !player.isPlaying && !player.isAlive) {
			destroyed.push(player);
			this._knownPlayers.splice(k--, 1);
		}
		else if(player.hasBeenModified) {
			modified.push(player);
			player.unmodify();
			nbrChars += player.name.length + 1;
		}
	}

	// Cellules créées
	for(k = 0; k < nbrCells; k++) {
		cell = this.cells[k];

		if(this._knownCells.indexOf(cell) === -1) {
			createdB.push(cell);
			this._knownCells.push(cell);
		}
	}

	// Cellules modifiées et détruites
	for(k = 0; k < this._knownCells.length; k++) {
		cell = this._knownCells[k];

		if(!cell.isAlive) {
			destroyedB.push(cell);
			this._knownCells.splice(k--, 1);
		}
		else if(cell.hasBeenModified) {
			modifiedB.push(cell);
		}
	}

	// Création du buffer
	bufferLength = 1 + 6 + created.length * 8 + modified.length * 4 + destroyed.length * 4 + nbrChars
		+ 6 + createdB.length * 19 + modifiedB.length * 14 + destroyedB.length * 14
		+ 1 + this._leaders.length * 4;
	dataView = new DataView(new Buffer(bufferLength));
	dataView.setUint8(c, 9); c += 1;

	// Ajout dans le bufffer des joueurs créés
	dataView.setUint16(c, created.length); c += 2;
	for(k = 0; k < created.length; k++) {
		player = created[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
		// name
		for(i = 0; i < player.name.length; i++) {
			dataView.setUint8(c, player.name.charCodeAt(i)); c += 1;
		}
		dataView.setUint8(c, 0); c += 1;
		// color
		dataView.setUint32(c, player.color); c += 4;
	}

	// Ajout dans le bufffer des joueurs modifiés
	dataView.setUint16(c, modified.length); c += 2;
	for(k = 0; k < modified.length; k++) {
		player = modified[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
		// name
		for(i = 0; i < player.name.length; i++) {
			dataView.setUint8(c, player.name.charCodeAt(i)); c += 1;
		}
		dataView.setUint8(c, 0); c += 1;
	}

	// Ajout dans le bufffer des joueurs détruits
	dataView.setUint16(c, destroyed.length); c += 2;
	for(k = 0; k < destroyed.length; k++) {
		player = destroyed[k];

		// id
		dataView.setUint32(c, player.id); c += 4;
	}
	
	// Ajout dans le buffer des cellules créées
	dataView.setUint16(c, createdB.length); c += 2;
	for(k = 0; k < createdB.length; k++) {
		cell = createdB[k];
		position = cell.position;
		player = cell.player;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// player's id
		dataView.setUint32(c, player ? player.id : 0); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// mass
		dataView.setUint16(c, cell.mass); c += 2;
		// isVirus
		dataView.setUint8(c, cell.isVirus()); c += 1;
	}

	// Ajout dans le buffer des cellules modifiées
	dataView.setUint16(c, modifiedB.length); c += 2;
	for(k = 0; k < modifiedB.length; k++) {
		cell = modifiedB[k];
		position = cell.position;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// x
		dataView.setUint16(c, cell.mass); c += 2;
	}

	// Ajout dans le buffer des cellules détruites
	dataView.setUint16(c, destroyedB.length); c += 2;
	for(k = 0; k < destroyedB.length; k++) {
		cell = destroyedB[k];
		position = cell.position;

		// id
		dataView.setUint32(c, cell.id); c += 4;
		// x
		dataView.setFloat32(c, position.x()); c += 4;
		// y
		dataView.setFloat32(c, position.y()); c += 4;
		// x
		dataView.setUint16(c, cell.mass); c += 2;
	}

	// Ajout au buffer des leaders
	dataView.setUint8(c, this._leaders.length); c += 1;
	for(k = 0; k < this._leaders.length; k++) {
		dataView.setUint32(c, this._leaders[k].id); c += 4;
	}

	return dataView.buffer;
};

module.exports = Game;