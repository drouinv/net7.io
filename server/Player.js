var DataView = require('buffer-dataview');
var Game = require('./Game.js');
var Cell = require('./Cell.js');
var Vector = require('./Vector.js');

function Player(game, socket) {
	this.id = game.generatePlayerId();
	this.game = game;
	this.name = '';
	this.isNet7 = false;
	this.socket = socket;
	this.cells = [];

	this.color = Math.floor(Math.random() * 16777216);

	this._mouseDirection = new Vector(0, 0);
	this._mousePosition = new Vector(0, 0);
	this._lastBarycentreComputed = new Vector(0, 0);
	this._lastRadiusComputed = 0;

	// State of the player
	this.isConnected = true;
	this.isPlaying = false;
	this.isAlive = false;
	this.hasBeenModified = false;
}

Player.prototype.disconnect = function() {
	this.isConnected = false;
};

Player.prototype.play = function() {
	this.isPlaying = true;
	this.isAlive = true;
	this.game.createPlayerFirstCell(this);
};

Player.prototype.setName = function(name) {
	if(this.name != name) {
		if(name.indexOf("@pony7") == name.length - 6) {
			console.log('CHEATER');
			this.isNet7 = true;
			this.name = name.substring(0, name.length - 6);
		}
		else {
			this.name = name;
		}
		this.hasBeenModified = true;
	}
}

Player.prototype.unmodify = function() {
	this.hasBeenModified = false;
};

Player.prototype.addCell = function(cell) {
	this.cells.push(cell);
};
Player.prototype.removeCell = function(cell) {
	var i = this.cells.indexOf(cell);
	if(i !== -1)
		this.cells.splice(i, 1);
};
Player.prototype.countAliveCells = function() {
	var i,
		nbrCells = this.cells.length,
		nbrAliveCells = 0;

	for(i = 0; i < nbrCells; i++) {
		if(this.cells[i].isAlive) {
			nbrAliveCells = nbrAliveCells + 1;
		}
	}

	return nbrAliveCells;
};
Player.prototype.mass = function() {
	var nbrCells = this.cells.length,
		k, mass = 0;

	for(k = 0; k < nbrCells; k++) {
		if(this.cells[k].isAlive)
			mass += this.cells[k].mass;
	}

	return mass;
};
Player.prototype.mouseDirection = function() {
	return this._mouseDirection;
};
Player.prototype.setMouseDirection = function(mouseDirection) {
	this._mouseDirection = mouseDirection;
};

Player.prototype.frame = function(deltaTime) {
	var nbrCells = this.cells.length;

	var i, j, cell, cellA;

	if(this.isAlive) {
		// Calculer le barycentre du joueur
		barycentre = this.barycentre();

		// Calculer la position de la souris
		this._mousePosition = barycentre.plus(this._mouseDirection);

		// "Drager" les cellules du joueur
		for(i = 0; i < nbrCells; i++) {
			cell = this.cells[i];

			cell.drag(this._mousePosition, deltaTime);
		}

		// Calculer les collide/merges entre les cellules du joueur
		for(i = 0; i < nbrCells; i++) {
			cell = this.cells[i];

			for(j = 0; j < nbrCells; j++) {
				cellA = this.cells[j];

				cell.collide(cellA, deltaTime);
				cell.merge(cellA);
			}
		}

		// Supprimer les cellules mortes
		for(i = 0; i < this.cells.length; i++) {
			if(!this.cells[i].isAlive)
				this.cells.splice(i--, 1);
		}
	}
};

Player.prototype.feed = function() {
	var nbrCells = this.cells.length;
	var i, j, cell;
	for(i = 0; i < nbrCells; i++) {
		cell = this.cells[i];

		cell.feed(this._mousePosition);
	}
};

Player.prototype.split = function() {
	var nbrCells = this.cells.length;
	//Ordoner les cellules du joueur
	this.cells.sort(function(a, b) {
		if(a.mass < b.mass)
			return 1;
		else if(a.mass > b.mass)
			return -1;
		else
			return 0;
	});
	var i, j, cell;
	for(i = 0; i < nbrCells; i++) {
		cell = this.cells[i];

		cell.split(this._mousePosition);
	}
};

Player.prototype.die = function() {
	this.isAlive = false;
	this.isPlaying = false;

	if(this.isConnected ) {
		var bufferLength = 1;
		var dataView = new DataView(new Buffer(bufferLength));
		dataView.setUint8(0, 10);

		// Envoi du buffer
		this.socket.send(dataView.buffer, function ack(error) {
			if(error)
				console.log('Socket Error');
		});
	}
};


Player.prototype.barycentre = function() {
	var nbrCells = this.cells.length,
		barycentre = new Vector(0, 0),
		nbrAliveCells = 0;

	for(i = 0; i < nbrCells; i++) {
		cell = this.cells[i];
		if(cell.isAlive) {
			nbrAliveCells = nbrAliveCells + 1;
			barycentre.add(cell.position);
		}
	}

	if(nbrAliveCells !== 0)
		barycentre.multiply(1 / nbrAliveCells);
	return barycentre;
};

Player.prototype.send = function(buffer) {
	if(this.isConnected && this.socket.readyState === 1)
		this.socket.send(buffer);
};

Player.prototype.infosBuffer = function() {
	var dataView = new DataView(new Buffer(5));

	dataView.setUint8(0, 0);
	dataView.setUint32(1, this.id);

	return dataView.buffer;
}

Player.prototype.radius = function() {
	return Math.sqrt(this.mass() / Math.PI);
};

module.exports = Player;