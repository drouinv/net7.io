var Game = require('./Game.js');
var Player = require('./Player.js');
var Vector = require('./Vector.js');

function Cell(game, player, position, mass, speed, isVirus) {
	this.id = game.generateCellId();
	this.game = game;
	this.player = player;
	this.position = position;
	this.mass = mass;
	this._isVirus = isVirus;

	this._speed = speed || new Vector(0, 0);
	this._maxSpeedNorm = this._speed.norm();
	this.isAlive = true;
	
	// About collisions
	this._collideStart = 0;
	this._maxCollideRemainingTime = 0;
	this._collideTimeout = null;

	this._mergeStart = 0;
	this._maxMergeRemainingTime = 0;
	this._mergeTimeout = null;
	this.hasBeenModified = true;
	this._sentCounter = 0;
}

Cell.prototype.setPosition = function(position) {
	this.position = position;
	// La cellule a été modifiée
	this.hasBeenModified = true;
};
Cell.prototype.setMass = function(mass) {
	this.mass = mass;
	// La cellule a été modifiée
	this.hasBeenModified = true;
};
Cell.prototype.isVirus = function() {
	return this._isVirus;
};
Cell.prototype.setIsVirus = function(isVirus) {
	this._isVirus = isVirus;
};
Cell.prototype.speed = function() {
	return this._speed;
};
Cell.prototype.setSpeed = function(speed) {
	this._speed = speed;
	this._maxSpeedNorm = speed.norm();
};
Cell.prototype.maxSpeedNorm = function() {
	return this._maxSpeedNorm;
};
Cell.prototype.setPlayer = function(player) {
	this.player = player;
	// La cellule a été modifiée
	this.hasBeenModified = true;
};
Cell.prototype.couldCollide = function() {
	return this._collideTimeout === null;
};
Cell.prototype.delayCollide = function(time) {
	if(this._collideTimeout === null) {
		var that = this;
		this._collideTimeout = setTimeout(function() {
			that._collideTimeout = null;
		}, time);
		this._collideStart = new Date();
		this._maxCollideRemainingTime = time;
	}
	else {
		var that = this;
		var timeElapsed = new Date() - this._collideStart;
		var remainingTime = this._maxCollideRemainingTime - timeElapsed;
		this._collideTimeout = setTimeout(function() {
			that._collideTimeout = null;
		}, time + remainingTime);
		this._collideStart = new Date();
		this._maxCollideRemainingTime = time + remainingTime;
	}
};
Cell.prototype.timeBeforeCollide = function() {
	if(this._collideTimeout === null)
		return 0;
	var timeElapsed = new Date() - this._collideStart;
	return this._maxCollideRemainingTime - timeElapsed;
};
Cell.prototype.couldMerge = function() {
	return this._mergeTimeout === null;
};
Cell.prototype.delayMerge = function(time) {
	if(this._mergeTimeout === null) {
		var that = this;
		this._mergeTimeout = setTimeout(function() {
			that._mergeTimeout = null;
		}, time);
		this._mergeStart = new Date();
		this._maxMergeRemainingTime = time;
	}
	else {
		var that = this;
		var timeElapsed = new Date() - this._mergeStart;
		var remainingTime = this._maxMergeRemainingTime - timeElapsed;
		this._mergeTimeout = setTimeout(function() {
			that._mergeTimeout = null;
		}, time + remainingTime);
		this._mergeStart = new Date();
		this._maxMergeRemainingTime = time + remainingTime;
	}
	
};
Cell.prototype.timeBeforeMerge = function() {
	if(this._mergeTimeout === null)
		return 0;
	var timeElapsed = new Date() - this._mergeStart;
	return this._maxMergeRemainingTime - timeElapsed;
};
Cell.prototype.isExhausted = function() {
	return this._isExhausted;
};
Cell.prototype.setIsExhausted = function(isExhausted) {
	this._isExhausted = isExhausted;
};
Cell.prototype.isJustSplited = function() {
	return this._isJustSplited;
};
Cell.prototype.setIsJustSplited = function(isJustSplited) {
	this._isJustSplited = isJustSplited;
};
Cell.prototype.setHasBeenModified = function(hasBeenModified) {
	this.hasBeenModified = hasBeenModified;
};

// Opérateurs internes
Cell.prototype.frame = function(deltaTime) {
	if(this._speed.norm() !== 0) {
		// Déplacer la cellule
		this.position.add(this._speed.times(deltaTime));

		// Replacer la cellule dans la limite du terrain
		this.moveInsideLimits();

		// Recalculer la vitesse de la cellule
		var diff = this._speed.clone(),
			diffNorm = this._maxSpeedNorm * deltaTime;
		if(diffNorm > this._speed.norm()) {
			this._speed.setX(0);
			this._speed.setY(0);
		}
		else {
			diff.scale(diffNorm);
			this._speed.substract(diff);
		}
		// La cellule a été modifiée
		this.hasBeenModified = true;
	} 
};

Cell.prototype.move = function(movement) {
	this.position.add(movement);
	this.moveInsideLimits();
	// La cellule a été modifiée
	this.hasBeenModified = true;
};

Cell.prototype.moveInsideLimits = function() {
	var halfWidth = this.game.width / 2;
	var halfHeight = this.game.height / 2;
	if(this.position.x() < -halfWidth)
		this.position.setX(-halfWidth);
	if(this.position.x() > halfWidth)
		this.position.setX(halfWidth);
	if(this.position.y() < -halfHeight)
		this.position.setY(-halfHeight);
	if(this.position.y() > halfHeight)
		this.position.setY(halfHeight);
	this.hasBeenModified = true;
}

Cell.prototype.drag = function(mousePosition, deltaTime) {
	var dragSpeed = 40 * Math.exp(- Math.log(this.mass) * 0.18);
	var dragDirection = mousePosition.minus(this.position);
	var radius3 = 3 * this.radius();
	if(dragDirection.norm() > radius3) {
		dragDirection.scale(radius3);
	}
	dragDirection.scale(dragSpeed * dragDirection.norm() / radius3 * deltaTime);
	this.position.add(dragDirection);
	this.moveInsideLimits();
	// La cellule a été modifiée
	this.hasBeenModified = true;
};

Cell.prototype.canEat = function(cell) {
	if(!cell.isAlive || ! this.isAlive)
		return false;

	// Deux cellules d'un même joueur
	if(this.player !== null && this.player === cell.player)
		return false;
	// Une CNJ non virus
	if(this.player === null && !this._isVirus)
		return false;
	// Une CNJ virus face à un joueur ou un virus
	if(this._isVirus && (cell.player !== null || cell.isVirus()))
		return false;

	if((this.mass * 0.8 <= cell.mass && (this.player === null || !this.player.isNet7)) || this.mass * 0.9 <= cell.mass)
		return false;

	// Recouvrement
	var recouvrement = this.player != null && this.player.isNet7 ? 0.5 : 0.5 + cell.mass / (this.mass + cell.mass);

	var distance = this.position.minus(cell.position).norm();
	return (this.radius() + cell.radius() - distance) / (2 * cell.radius()) > recouvrement;
};

Cell.prototype.eat = function(cell) {
	if(this.canEat(cell)) {
		this.mass += cell.mass;

		if(cell.isVirus() && !this._isVirus) {
			this.explode();
		}
		else if(this._isVirus && this.mass >= 200) {
			this.split(cell.position.plus(cell.speed()));
		}

		cell.die(this.position.clone(), this.mass);
		// La cellule a été modifiée
		this.hasBeenModified = true;
	}
};

Cell.prototype.explode = function() {
	var k, direction = new Vector(), position, positionOffsetNorm,
		speed;
	var nbrAliveCells = this.player.countAliveCells();
	var massExplode = Math.max(17, this.mass * 0.05);

	while(nbrAliveCells < 16 && this.mass >= 17 + massExplode) {
		nbrAliveCells = nbrAliveCells + 1;
		this.mass -= massExplode;

		direction = new Vector(Math.random() * 2 - 1, Math.random() * 2 - 1);
		position = this.position.clone();
		speed = direction.scaled(100);

		var explode = new Cell(this.game, this.player, position, massExplode, speed, false);
		explode.move(direction.scaled(this.radius() + explode.radius()));
		var time = this.player !== null && this.player.isNet7 ? 5000 : 10000;
		explode.delayMerge(time);

		this.player.addCell(explode);
		this.game.addCell(explode);
	}
	// La cellule a été modifiée
	this.hasBeenModified = true;
};

Cell.prototype.canFeed = function() {

	return this.mass >=  34 && this.isAlive;
};

Cell.prototype.feed = function(mousePosition) {
	if(this.canFeed()) {
		this.mass -= 17;
		var dragDirection = mousePosition.minus(this.position);

		var speed = dragDirection.scaled(96);

		var feed = new Cell(this.game, null, this.position.clone(), 16, speed, false);
		feed.move(dragDirection.scaled(feed.radius() + this.radius()));

		this.game.addCell(feed);
		// La cellule a été modifiée
		this.hasBeenModified = true;
	}
};

Cell.prototype.canSplit = function() {
	if(this.player === null)
		return true;
	return this.mass >= 34 && this.isAlive && this.player.countAliveCells() < 16;
};

Cell.prototype.split = function(mousePosition) {
	if(this.canSplit()) {
		this.mass = Math.floor(this.mass / 2);
		var dragDirection = mousePosition.minus(this.position).normalize();

		var position = dragDirection.plus(this.position);
		var speed = dragDirection.scaled(128);

		var split = new Cell(this.game, this.player, position, this.mass, speed, this._isVirus);

		split.delayCollide(1000);
		var time = this.player !== null && this.player.isNet7 ? 20000 : 30000 + split.mass + this.timeBeforeMerge();
		split.delayMerge(time);

		if(this.player !== null)
			this.player.addCell(split);
		this.game.addCell(split);
		// La cellule a été modifiée
		this.hasBeenModified = true;
	}
};

Cell.prototype.canCollide = function(cell) {
	if(this.player === null)
		return false;
	if(this.player !== cell.player)
		return false;
	if(this === cell)
		return false;
	if(!this.couldCollide() || !cell.couldCollide())
		return false;
	if(this.couldMerge() && cell.couldMerge())
		return false;
	if(!this.isAlive || !cell.isAlive)
		return false;

	// Recouvrement
	var distance = this.position.minus(cell.position).norm();
	return this.radius() + cell.radius() - distance > 0;
};

Cell.prototype.collide = function(cell, deltaTime) {
	if(this.canCollide(cell)) {
		var direction = cell.position.minus(this.position);
		var sumRadius = this.radius() + cell.radius();
		var cover = (sumRadius - direction.norm());

		var pulse = Math.min(cover, 500 * deltaTime * (1 - cover * (-0.5) / sumRadius));
		
		direction.scale(pulse);
		var massSum = this.mass + cell.mass;
		var thisMassRatio = this.mass / massSum;
		var cellMassRatio = cell.mass / massSum;
		cell.move(direction.times(thisMassRatio));
		this.move(direction.times(- cellMassRatio));
		// La cellule a été modifiée
		this.hasBeenModified = true;
	};
};

Cell.prototype.canMerge = function(cell) {
	if(this.player === null)
		return false;
	if(this.player !== cell.player)
		return false;
	if(this === cell)
		return false;
	if(!this.couldMerge() || !cell.couldMerge())
		return false;
	if(!this.isAlive || !cell.isAlive)
		return false;

	// Recouvrement
	var recouvrement = cell.mass / (this.mass + cell.mass);

	var distance = this.position.minus(cell.position).norm();
	return (this.radius() + cell.radius() - distance) / (2 * cell.radius()) > recouvrement;
};

Cell.prototype.merge = function(cell) {
	if(this.canMerge(cell)) {
		var massSum = this.mass + cell.mass;
		var thisMassRatio = this.mass / massSum;
		var cellMassRatio = cell.mass / massSum;
		// Placer la cellule au centre de masse
		var thisPositionPart = this.position.times(thisMassRatio),
			cellPositionPart = cell.position.times(cellMassRatio);
		this.position = thisPositionPart.add(cellPositionPart);
		this.mass += cell.mass;
		var time = this.player !== null && this.player.isNet7 ? 500 : 1000;
		this.delayMerge(time);

		cell.die(this.position.clone(), this.mass);
		// La cellule a été modifiée
		this.hasBeenModified = true;
	};
};

Cell.prototype.die = function(position, mass) {
	this.position = position;
	this.mass = mass;
	this.isAlive = false;
	if(this.player !== null && this.player.countAliveCells() === 0)
		this.player.die();
	// La cellule a été modifiée
	this.hasBeenModified = true;
};

// Opérateurs externes
Cell.prototype.radius = function() {
	return Math.sqrt(this.mass / Math.PI);
};

Cell.prototype.distance = function(cell) {
	return this.position.minus(cell.position).norm();
}

module.exports = Cell;