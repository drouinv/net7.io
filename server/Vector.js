function Vector(x, y) {
	this._x = x;
	this._y = y;
	this._norm;
	this._modified = true;
}

// Accesseurs et mutateurs
Vector.prototype.x = function() {

	return this._x;
};
Vector.prototype.y = function() {

	return this._y;
};
Vector.prototype.setX = function(x) {
	this._x = x;
	this._modified = true;
};
Vector.prototype.setY = function(y) {
	this._y = y;
	this._modified = true;
};
Vector.prototype.norm = function() {
	if(this._modified) {
		this._modified = false;
		this._norm = Math.sqrt(this._x * this._x + this._y * this._y);
	}
	return this._norm;
};
Vector.prototype.isNull = function() {

	return this._x == 0 && this._y == 0;
};

// Opérateurs internes
Vector.prototype.copy = function(vector) {
	this._x = vector.x();
	this._y = vector.y();
	this._modified = false;
	this._norm = vector.norm();
	return this;
};
Vector.prototype.add = function(vector) {
	this._x += vector.x();
	this._y += vector.y();
	this._modified = true;
	return this;
};
Vector.prototype.substract = function(vector) {
	this._x -= vector.x();
	this._y -= vector.y();
	this._modified = true;
	return this;
};
Vector.prototype.multiply = function(a) {
	this._x *= a;
	this._y *= a;
	this._norm *= a;
	return this;
};
Vector.prototype.scale = function(a) {
	if(this._modified) {
		this._modified = false;
		this._norm = Math.sqrt(this._x * this._x + this._y * this._y);
	}
	if(this._norm === 0) {
		this._x  = 1;
		this._norm = 1;
	}
	var ratio = a / this._norm;
	this._x *= ratio;
	this._y *= ratio;
	this._norm = a;
	this._modified = false;
	return this;
};
Vector.prototype.normalize = function() {
	if(this._modified) {
		this._modified = false;
		this._norm = Math.sqrt(this._x * this._x + this._y * this._y);
	}
	if(this._norm === 0) {
		this._x  = 1;
		this._norm = 1;
	}
	else {
		this._x /= this._norm;
		this._y /= this._norm;
		this._norm = 1;
	}
	return this;
};
Vector.prototype.reverse = function() {
	this._x = - this._x;
	this._y = - this._y;
	return this;
}

// Opérateurs externes
Vector.prototype.clone = function() {

	return new Vector(this._x, this._y);
};
Vector.prototype.plus = function(vector) {

	return new Vector(this._x + vector.x(), this._y + vector.y());
};
Vector.prototype.minus = function(vector) {

	return new Vector(this._x - vector.x(), this._y - vector.y());
};
Vector.prototype.times = function(a) {

	return new Vector(this._x * a, this._y * a);
};
Vector.prototype.scaled = function(a) {
	var newVector = this.clone();
	newVector.scale(a);
	return newVector;
};
Vector.prototype.normalized = function() {
	var newVector = this.clone();
	newVector.normalize();
	return newVector;
};
Vector.prototype.reversed = function() {
	return new Vector(- this._x, - this._y);
};

module.exports = Vector;